(function(){
    var TaskApp = angular.module("TaskApp", []);

    var TaskCtrl=function(){
        var TaskCtrl=this;
        TaskCtrl.task="";
        TaskCtrl.duedate=""; //input
        TaskCtrl.formattedDate=""; //output
        TaskCtrl.category="";
        TaskCtrl.status="";
        TaskCtrl.array=[];
        TaskCtrl.categoryLabel=["Uncategorized", "Full Stack Foundation", "Personal Development", "Errands" ];

// populate with sample data 
        TaskCtrl.array = [  {task:"Watch Youtube Video on Hair Styling",duedate:"02/18/2017", formattedDate:"2/18/2017", category:"Personal Development", status:"Pending" },
                            {task:"Complete group assignment",duedate:"04/18/2017",formattedDate:"4/18/2017", category:"Full Stack Foundation",status:"Pending"  },
                            {task:"Buy movie tickets",duedate:"03/20/2017", formattedDate:"3/20/2017",category:"Errands", status:"Pending" },
                            {task:"Set up an in-kitchen hydroponics farm",duedate:"2/20/2017", formattedDate:"2/20/2017",category:"Errands", status:"Pending" },
                            {task:"Buy tea for everyone",duedate:"03/05/2017", formattedDate:"3/05/2017",category:"Full Stack Foundation", status:"Pending" }
        ];
// adding a new task into the array of tasks
    TaskCtrl.add = function(){
            
            TaskCtrl.formattedDate = TaskCtrl.duedate.toLocaleDateString();

            TaskCtrl.array.push( {
                task:TaskCtrl.task, 
                category:TaskCtrl.category, 
                duedate:TaskCtrl.duedate, 
                formattedDate:TaskCtrl.formattedDate,
                status: TaskCtrl.status  
            } );
            console.log(TaskCtrl.array);
    };    
    
    
// removing a task from the array of tasks
    TaskCtrl.delete = function(clickindex){		
       	if( clickindex === -1 ) {
			alert( "Something gone wrong" );
		}
        console.log(clickindex);
		TaskCtrl.array.splice( clickindex, 1 );		
	};

// edit the Task selected
    TaskCtrl.edit = function(clickindex) {
        TaskCtrl.array[clickindex] = {task:" ",duedate:"02/18/2017", formattedDate:"2/18/2017", category:" ", status:" " }


    };


// mark complete for the task selected
    TaskCtrl.completed = function(clickindex) {
            TaskCtrl.array[clickindex].status = "Done";

    };




    // TaskCtrl.delete=function(placeholder){  
    //     var idx=TaskCtrl.array.findIndex(function(elem){
    //         return(placeholder == elem.task)
    //     })
    //     if (idx>=0){
    //         TaskCtrl.array.splice(idx,1);
    //     console.log(placeholder);
    //     }
         
    //         //splice(3,1,) means go to the 4th basket and remove 1, starting from it
    // }

///the } below belongs to the controller

    }

    TaskApp.controller("TaskCtrl", [TaskCtrl])

})();